/*
 * Sample program for DokodeModem
 * Read ADC
 * Copyright (c) 2024 Circuit Desgin,Inc
 * Released under the MIT license
 */
#include <dokodemo.h>

DOKODEMO Dm = DOKODEMO();

void setup()
{
  Dm.begin(); // 初期化が必要です。

  // デバッグ用　USB-Uart
  SerialDebug.begin(115200);
}

void loop()
{
  // 精密な値が必要であれば補正をお勧めします。
  // １回読みでは不安定ですので、３回ほど読んで平均します
  float adc = 0.0;
  for (int i = 0; i < 3; i++)
  {
    adc += Dm.readExADC();
  }
  adc /= 3.0;

  // 小数点以下２で表示します。
  SerialDebug.println("Adc:" + String(adc, 2));

  delay(3000);
}
